// $Id$

Zenbreeze
---------

Zeenbreeze is a Zen sub-theme port of Bluebreeze theme.

- INSTALLATION -

 1. Download Zen-6.x-2.0 from http://drupal.org/project/zen
 
    Zeenbreeze is known to work with zen-6.x-2.0-beta1
    
 2. Unpack the downloaded file under the following locations:
 
    sites/all/themes
    
 3. 
 
- FEATURES -
